package com.fetchData.sample.Example;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name ="employee_details")
public class UserModel {
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="employee_name")
String employee_name;
	@Column(name="age")
Integer age;
	@Column(name="date_of_birth")
String date_of_birth;
	@Column(name="project_name")
String project_name;
	@Column(name="employer_name")
String employer_name;
	@Column(name="salary")
Integer salary;
	@Column(name="years_of_experience")
Integer years_of_experience;

	public Integer getAge() {
		return age;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}public String getEmployee_name() {
		return employee_name;
	}public String getEmployer_name() {
		return employer_name;
	}public String getProject_name() {
		return project_name;
	}public Integer getSalary() {
		return salary;
	}public Integer getYears_of_experience() {
		return years_of_experience;
	}public void setAge(Integer age) {
		this.age = age;
	}public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}public void setEmployer_name(String employer_name) {
		this.employer_name = employer_name;
	}public void setProject_name(String project_name) {
		this.project_name = project_name;
	}public void setSalary(Integer salary) {
		this.salary = salary;
	}public void setYears_of_experience(Integer years_of_experience) {
		this.years_of_experience = years_of_experience;
	}


}