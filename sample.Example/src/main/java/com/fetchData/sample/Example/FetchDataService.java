package com.fetchData.sample.Example;
//import org.springframework.data.repository.CrudRepository;
//import com.fetchData.sample.Example.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
//public interface UserRepository extends CrudRepository<User, Integer> {

//}
import java.util.List;

@Repository
public interface FetchDataService extends JpaRepository<UserModel,Integer>{

	@Override
	List<UserModel> findAll();
}
